dico_correspondance = {}

dico_correspondance['adresse_titulaire'] = "adresse"
dico_correspondance['carrosserie'] = "carrosserie"
dico_correspondance['categorie'] = "categorie"
dico_correspondance['couleur'] = "couleur"
dico_correspondance['cylindree'] = "cylindree"
dico_correspondance['date_immatriculation'] = "date_immat"
dico_correspondance['denomination_commerciale'] = "denomination"
dico_correspondance['energie'] = "energy"
dico_correspondance['prenom'] = "firstname"
dico_correspondance['immatriculation'] = "immat"
dico_correspondance['marque'] = "marque"
dico_correspondance['places'] = "places"
dico_correspondance['poids'] = "poids"
dico_correspondance['puissance'] = "puissance"

dico_correspondance['type'] = "type_variante_version"
dico_correspondance['variante'] = "type_variante_version"
dico_correspondance['version'] = "type_variante_version"

dico_correspondance['vin'] = "vin"